#include "queue.h"
#define MARCA 2

int main (int argc, char *argv[]) {

   if (argc < 2) {
      printf("Uso: %s image_de_entrada.pbm\n", argv[0]);
      exit(1);
   }

   int ncolunas, nlinhas;

   char *type = (char *)malloc(sizeof(char));

   /*Abrindo o arquivo que contem a imagem*/
   FILE *arq_imagem_entrada = fopen(argv[1], "r");

   /*Lendo o cabecalho da imagem*/
   fscanf(arq_imagem_entrada, "%s", type);

   fscanf(arq_imagem_entrada, "%d %d", &ncolunas, &nlinhas);

   /*alocacao dinamica de memoria para a matriz que ira guardar a imagem*/
   int **matriz = alocando_matriz (ncolunas, nlinhas);

   /*Lendo a imagem*/
   int i, j;
   for (i = 0; i < nlinhas; i++) {
      for (j = 0; j < ncolunas; j++) {
          fscanf(arq_imagem_entrada, "%d", &matriz[i][j]);
      }
   }


   /*Fechando o arquivo da imagem de entrada*/
   fclose(arq_imagem_entrada);
   printf("Existem %d componentes na imagem %s\n", contaObjetos(matriz, nlinhas, ncolunas), argv[1]);

  for (i=0; i<nlinhas; i++)
    free (matriz[i]); /* libera as linhas da matriz */
  free (matriz);

  return 0;
}

int ** alocando_matriz (int ncolunas, int nlinhas) {

   int i;

   int **matriz = (int **)malloc(nlinhas * sizeof(int *));

   for (i = 0; i < nlinhas; i++) {
      matriz[i] = (int *)malloc(ncolunas * sizeof(int));
   }
   return matriz;
}



int contaObjetos(int **imagem, int altura, int largura)
{
    int i, j, cont = 0, x, y, k, m;
    Queue *fila = create_queue();//Cria fila


    for(i=1; i<altura-1; i++)//Percorre a matriz da imagem (aluta-1 e largura-1 foram colocados para não acessar posições indevidas)
    {
        for(j=1; j<largura-1; j++)
        {

            if(imagem[i][j]==1)//
            {
                imagem[i][j]=MARCA;
		        fila = enqueue(fila, i, j);
		        cont++;
            }
            while(!Empty(fila))
            {
                x = frontX(fila);//Guarda o X do primeiro da fila
                y = frontY(fila);//Guarda o Y do primeiro da fila
                fila = dequeue(fila);//Tiramos o primeiro elemento da fila

               for(k=x-1; k<=x+1; k++)//Percorre as 8 posições desejadas, inicia uma posição antes do x e termina uma depois
                {
                    for(m=y-1; m<=y+1; m++)//Inicia uma posição antes do y e termina uma depois
                    {
                        if(k!=x || m!=y)//Caso fosse difrente da posição atual
                        {
                            if(imagem[k][m]==1)//Se o pixel tivesse valor 1 (preto)
                            {
                                imagem[k][m] = MARCA; //Marcamos como visitado
                                fila = enqueue(fila, k, m);//Colocamos o mesmo na fila
                            }
                        }
                    }
                }
            }


        }
    }
    free_queue (fila);//Free da fila

    return cont;//retornamos o contador(número de componentes)
}
