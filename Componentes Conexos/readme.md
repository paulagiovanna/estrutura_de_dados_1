### Autores do projeto
Paula Giovanna Rodrigues e Victor Antonio Menuzzo (github.com/victormenuzzo)

### Proposta do projeto
Desenvolver um programa que utilize estruras de dados vistas na matéria - como pilhas e filas - para contar a quantidade de componentes conexos em uma imagem com extensão .pbm.

### Como executar
Para rodar o programa em seu linux abra o terminal na pasta e digite o seguinte comando:
```
~$ make
```
