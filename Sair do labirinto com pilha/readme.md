### Autores do projeto
Paula Giovanna Rodrigues e Victor Antonio Menuzzo (github.com/victormenuzzo)

### Proposta do projeto
Proposta do projeto: Desenvolver um programa que utilize a estrutura de dados de pilhas para fazer com que dado um labirinto um robo possa computar seus caminhos e encontrar uma saída. Caso ele vá para um caminho que acabe em 'parede' deve voltar ao ultimo caminho não escolhido e todas suas escolhas devem estar ilustradas no arquivo saida.txt.

### Como executar
Para rodar o programa em seu linux abra o terminal na pasta e digite o seguinte comando:
```
~$ make
```

Dica: Brinque com o programa alterando o arquivo 'labirinto.txt':

A primeira linha deve indicar o numero de linhas e colunas que ele terá nesse formato - linhas TAB colunas

As demais linhas representam o labirinto, separe cada numero com um TAB e use - 1 para parede, 2 para local inicial do robô e 3 para o destino.
