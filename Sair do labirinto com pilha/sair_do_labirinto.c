#include "stack.h"

#include <stdio.h>
#define MARCA 7
#define FIM 3
#define INICIO 2
#define PAREDE 1
#define LIVRE 0


int main (int argc, char *argv[]){
	 if (argc < 3) {
                printf("Uso: %s labirinto_de_entrada.txt arquivo_de_saida.txt\n", argv[0]);
                exit(1);
  	}

	int i, j, linhas, colunas, **matriz, inicioX, inicioY;

	FILE *file, *saida;
	file=fopen(argv[1], "r");//arquivo de leitura
	saida=fopen(argv[2], "w");//arquivo editável


	fscanf(file, "%d %d", &linhas, &colunas);//Le a primeira linha do txt e recebe a dimensão da matriz

	matriz = (int**)malloc(linhas * sizeof(int*)); //Aloca um Vetor de Ponteiros

  	for (i = 0; i < linhas; i++){ //Percorre as linhas do Vetor de Ponteiros
       matriz[i] = (int*) malloc(colunas * sizeof(int)); //Aloca um Vetor de Inteiros para cada posição do Vetor de Ponteiros.
       for (j = 0; j < colunas; j++){ //Percorre o Vetor de Inteiros atual.
            matriz[i][j] = 0; //Inicializa com 0.
       }
  	}

	for(i=0; i<linhas; i++){//Percorre as linhas obtidas no fscanf anterior, coloca os valores do txt na matriz
		for(j=0; j<colunas; j++){
			fscanf(file, "%d", &matriz[i][j]);
		}
	}

	for(i=0; i<linhas; i++){ //Percorre a matriz buscando seu inicio
		for(j=0; j<colunas; j++){
			if(matriz[i][j]==INICIO){
				inicioX=i;
				inicioY=j;
			}
		}
	}


	achaFim(matriz,linhas, colunas, inicioX, inicioY, saida);//Função para percorrer o labirinto.

	for (i=0; i<linhas; i++)
		free (matriz[i]); /* libera as linhas da matriz */
  	free (matriz);

  	return 0;
}

void achaFim(int **labirinto, int altura, int largura, int inicioX, int inicioY, FILE *saida)
{
    Stack *pilha = create_stack(), *aux;
    int  x, y, cont=0, flag=0, fimX, fimY;


    pilha=push(pilha, inicioX, inicioY);//Colocamos na pilha a posição inicial

   	fprintf(saida, "\nPasso a Passo do robô para encontrar a princesa:\n");

	 while(!Empty(pilha))
    {
        x = topX(pilha);//Guardamos as coordenadas da posição
        y = topY(pilha);
        pilha=pop(pilha);//Tiramos a posição da pilha

        if(labirinto[x][y] == FIM)//caso encontre o fim
        {
            printf("\n\nCaminho encontrado! Conferir arquivo .txt de saida!\n\n\n");
            fimX=x;//Variáveis criadas apenas para printar um 💛 no final
            fimY=y;
            pilha=free_stack(pilha);//Desalocamos a pilha
            flag = 1;//Flag para saber se conseguiu achar uma saída ou não
        }

        else//Enquanto nao encontrasse o fim, percorremos as posições vizinhas
        {
            //Caso fosse diferente de parede ou marca colocamos a posição na fila
            if(labirinto[x-1][y] != PAREDE && labirinto[x-1][y] != MARCA)
            {
                pilha = push(pilha, x-1, y);
            }
            if(labirinto[x+1][y] != PAREDE && labirinto[x+1][y] != MARCA)
            {
                pilha=push(pilha, x+1, y);
            }
            if(labirinto[x][y-1] != PAREDE && labirinto[x][y-1] != MARCA)
            {
                pilha=push(pilha, x, y-1);
            }
            if(labirinto[x][y+1] != PAREDE && labirinto[x][y+1] != MARCA)
            {
                pilha=push(pilha, x, y+1);
            }
        }
        labirinto[x][y] = MARCA;//Marcamos a posição como visitada
        cont++;//Contador para saber o número de passos
        fprintf(saida, "\n\nPasso %d\n", cont);
        for(int i=0; i<altura; i++)//Percorre a Matriz printado no txt o labirinto
        {
           for(int j = 0; j<largura; j++)
            {
                if(labirinto[i][j]==PAREDE)//Quando for parede printamos uma bomba
                	fprintf(saida, "💣\t");
            	else if(labirinto[i][j]==FIM)//No fim printamos uma princesa
            		fprintf(saida, "👸\t");
            	else if(i==fimX && j==fimY)//Printamos um coração caso encontre
            		fprintf(saida, "💛\t");
            	else if(i == x && j == y)//Printamos os passos do robo
            		fprintf(saida, "🤖\t");
            	else if(labirinto[i][j]==LIVRE || labirinto[i][j]==MARCA)
            		fprintf(saida, " \t");

  			}
            fprintf(saida, "\n");
        }
    }
    if(flag == 0)//Caso não encontre caminho
    {
    	printf("\n\nNão há caminho disponivel, confira o .txt de saida\n\n\n");
    	fprintf(saida, "\n O robô não pode chegar até a princesa 😭😭😭😭😭😭\n");
    }
}
