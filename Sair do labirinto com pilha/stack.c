#include "stack.h"

/*Função para criar uma lista encadeada que tem comportamento de um TAD Pilha.*/
Stack* create_stack () {
   return NULL;
}

/*Função para desalocar uma lista encadeada.*/
Stack* free_stack (Stack *s) {
   while (s != NULL) {
      Stack *aux = s->next; /*guarda ref. p/ prox.*/
      free (s); /*libera a memória.*/
      s = aux; /*faz lista apontar p/ o prox. elem.*/
   }
   return s;
}

/*Função para empilhar um elemento. A posição do novo elemento é o início da lista encadeada. */
Stack* push (Stack *s, int x, int y) {
   /*Criando novo elemento: */
   Stack *n = (Stack *)malloc(sizeof(Stack));
   n->x = x; /*inserindo o dado.*/
   n->y = y;
   n->next = s; /*encadeamento da lista antiga no novo nó.*/
   return n; /*retornando nova cabeça.*/
}

/*Função para desempilhar um elemento do início da lista encadeada. */
Stack* pop (Stack *s) {
   if (!Empty(s)) {
      Stack *aux = s->next; /*Guarda endereço do próximo elemento.*/
      free (s); /*Removendo nodo da lista.*/
      return aux; /*Nova cabeça da lista!*/
   }
   else {
      return NULL; /*Remoção em uma lista vazia.*/
   }
}

/*Função para mostrar sem desempilhar um elemento do início da lista encadeada.*/
int topX (Stack *s) {
   if (!Empty(s)) {
      return s->x;      
   }
   //return NOT_FOUND; 
}


int topY (Stack *s) {
   if (!Empty(s)) {
      return s->y;      
   }

   //return NOT_FOUND; 
}

/*Função para testar se existem elementos na lista encadeada.*/
int Empty (Stack *s){
   return (s == NULL);
}

void imprime(Stack *s){
	Stack *aux;
	for(aux=s; aux!=NULL; aux=aux->next){
		printf("x:%d\t", s->x);
		printf("y:%d\n", s->y);
	}	

}
