#include <stdio.h>
#include <stdlib.h>

typedef struct _stack {
   int x;
   int y;
   struct _stack *next; 
} Stack;

Stack* create_stack ();

Stack* free_stack (Stack *s);

Stack* push (Stack *s, int x, int y);

Stack* pop (Stack *s);

int topX (Stack *s);

int topY (Stack *s);

int Empty (Stack *s);

int **alocando_matriz (int ncolunas, int nlinhas);

void achaFim(int **labirinto, int altura, int largura, int inicioX, int inicioY, FILE *saida);
