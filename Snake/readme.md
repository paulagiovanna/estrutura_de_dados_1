### Autores do Projeto
Paula Giovanna Rodrigues e Victor Antonio Menuzzo (github.com/victormenuzzo)

### Proposta do projeto
Desenvolver um jogo snake que rode no terminal do linux e retome alguns dos principais assuntos vistos na disciplina - ex. Ordenação, Busca, Fila, Pilha, etc. Em que o corpo da cobra seja composto por 'S', as maçãs que ela come sejam 'M' e hajam comidas bônus 'B' que dão mais pontos para o jogador sem crescer o corpo da cobra. Além do jogo deve haver um ranking local que pode ser guardado em um .txt e acessado no terminal.

### Como executar
Para rodar o snake no seu linux é necessário possuir a biblioteca ncurses.h, para adquiri-la basta digitar o seguinte comando no terminal:

```
~$ sudo apt-get install libncurses5-dev libncursesw5-dev
```

Após instalar a biblioteca, abra o terminal na pasta do snake e digite o seguinte comando:

```
~$ make
```

E manipule a cobra com as teclas ← ↑ → ↓

